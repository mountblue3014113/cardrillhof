//The accounting team needs all the years from every car on the lot. Execute a function
//that will return an array from the dealer data containing only the car years and log
//the result in the console as it was returned.

const getCarYears = (inventory) => {
  if (!Array.isArray(inventory)) {
    return "Inventory must be an array!";
  }
  if (inventory.length === 0) {
    return "Inventory is empty !";
  }
  const carYears = inventory.map((years) => years.car_year);
  return carYears;
};
module.exports = getCarYears;
