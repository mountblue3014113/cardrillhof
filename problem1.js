// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function
//that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
const findCarById = (inventory, id) => {
  if (Array.isArray(inventory) && typeof id === "number") {
    if (inventory.length === 0) {
      return `inventory is empty`;
    }
    const carWithId = inventory.filter((car) => car.id === id);
    if (carWithId) {
      return `Car ${carWithId[0].id} is a ${carWithId[0].car_year} ${carWithId[0].car_make} ${carWithId[0].car_model}`;
    } else {
      return `Car with id ${id} not found.`;
    }
  } else {
    return [];
  }
};
module.exports = findCarById;
