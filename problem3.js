// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order
// and log the results in the console as it was returned.
const caseSensitiveSort = (a, b) => {
  return a.localeCompare(b);
};

const sortCarModelsAlphabetically = (inventory) => {
  if (!Array.isArray(inventory)) {
    return "Inventory must be an array !";
  }
  if (inventory.length === 0) {
    return "Inventory is empty !";
  }

  const carModels = inventory.map((car) => car.car_model);
  const sortedCarModels = carModels.sort(caseSensitiveSort);
  return sortedCarModels;
};
module.exports = sortCarModelsAlphabetically;
