// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory
// is?  Log the make and model into the console in the format of:
const findLastCar = (inventory) => {
  if (Array.isArray(inventory)) {
    if (inventory.length === 0) {
      return `Inventory is Empty !`;
    }
    const lastCar = inventory.reduce((acc, car) => car, null);
    return `Last car is a ${lastCar.car_make} ${lastCar.car_model}`;
  } else {
    return [];
  }
}
module.exports = findLastCar;

