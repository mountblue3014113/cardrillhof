// // ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.
//Execute a function and return an array that only contains BMW and Audi cars.
//Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const bmwAndAudiCars = (inventory) => {
  if (!Array.isArray(inventory)) {
    return "Inventory must be an array!";
  }
  if (inventory.length === 0) {
    return "Inventory is empty !";
  }
  const onlyBMWAndAudi = inventory.filter((make) => make.car_make === "BMW" || make.car_make === "Audi"
  );

  if (onlyBMWAndAudi.length === 0) {
    return `No BMW and Audi cars in the inventory !`;
  } else {
    return onlyBMWAndAudi;
  }
};
module.exports = bmwAndAudiCars;
