// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000.
// Using the array you just obtained from the previous problem, find out how many
// cars were made before the year 2000 and return the array of older cars and log its length.
const getCarYears = require("./problem4.js");
const carsOlderThan2000 = (inventory) => {
  if (!Array.isArray(inventory)) {
    return "Inventory must be an array!";
  }
  if (inventory.length === 0) {
    return "Inventory is empty !";
  }

  const getYears = getCarYears(inventory).filter((year) => year < 2000);
  if (getYears.length != 0) {
    return getYears.length;
  } else {
    return `There's no cars made before the year 2000`;
  }
};
module.exports = carsOlderThan2000;
